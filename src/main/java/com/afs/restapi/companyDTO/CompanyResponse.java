package com.afs.restapi.companyDTO;

import com.afs.restapi.employeeDTO.EmployeeResponse;
import com.afs.restapi.entity.Employee;
import com.afs.restapi.mapper.EmployeeMapper;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

public class CompanyResponse {

    private Long id;
    private String name;
    private List<EmployeeResponse> employees = new ArrayList<>();

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public List<EmployeeResponse> getEmployees() {
        return employees;
    }

    public void setEmployees(List<Employee> employees) {
        EmployeeMapper employeeMapper = new EmployeeMapper();
        this.employees = employees.stream()
                .map(employee -> employeeMapper.toResponse(employee))
                .collect(Collectors.toList());
    }
}
